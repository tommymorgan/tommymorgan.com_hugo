---
title: "Conflicted"
date: 2019-03-14T00:23:42-06:00
tags: [Deep Thoughts]
---
When I’m stuck in highway traffic that suddenly eases for no apparent reason I don’t know whether to be relieved or angry.