---
title: "Price Gouging on Amazon"
date: 2020-04-24T00:20:43-06:00
tags: [Technology]
---
As a result of the COVID-19 lockdown my family is having to pass our one webcam around and sometimes we have to prioritize who will get it and who will have no video. We have a Logitech c920 which has an MSRP of $79. I see it listed for more than 4x the MSRP on Amazon (and Newegg). This is against Amazon’s policies, but if you can figure out how to report it, you’re a better searcher than me. Amazon could easily add a button for this. And should. These sellers deserve legal consequences, not riches.