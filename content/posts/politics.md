---
title: "Politics"
date: 2020-06-29T00:28:10-06:00
tags: [Other]
---
noun. A process by which decisions are made by committee without the burden of facts.